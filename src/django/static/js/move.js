function Move(nodes) {

	var pointArray = [];

	for(var i = 0; i < nodes.length; i++) {
		var wait = nodes[i].waitTime * 1000;
		var distance = 0;

		for(var j=0; j < nodes[i].shape.length -1;j++) {
			distance = distance + parseFloat(MoveUtil.getLatLon(nodes[i].shape[j]).distanceTo(MoveUtil.getLatLon(nodes[i].shape[j + 1])) * 60 * 60);
		}

		var speed = distance / nodes[i].travelTime;

		for(var j=0; j < nodes[i].shape.length -1;j++) {
			pointArray.push({ "point": nodes[i].shape[j], "wait": wait, "speed": speed});
			wait = 0;
		}
	}

	pointArray.push({ "point": nodes[nodes.length -1].shape[nodes[nodes.length -1].shape.length -1 ], "wait": 0, "speed": 0});

	this._nodes = [];
	for (var i = 0; i < pointArray.length; i++) {
		pointArray[i].point = MoveUtil.getLatLon(pointArray[i].point);
		this._nodes.push(pointArray[i]);
	}

	this._lastNodeIndex = 0;
	this._lastNode = this._nodes[0];
	this._bearing = this._nodes[0].point.bearingTo(this._nodes[1].point);
	this._moving = false;
	this._moveStart = -1;
	this._speedFactor = 1;
};

Move.prototype.getCurrentLocation = function () {
	if (this._moving) {
		this.saveState();
	}

	return this._lastNode.point;

};

Move.prototype.toggleMove = function() {
	if(this._moving) {
		this.stop();
	} else {
		this.start();
	}
}

Move.prototype.start = function () {
	if (this._moving) {
		return;
	}

	this._moving = true;
	this._moveStart = new Date();
};

Move.prototype.stop = function () {
	if (!this._moving) {
		return;
	}

	this.saveState();
	this._moving = false;
	this._moveStart = -1;
};

Move.prototype.setSpeedFactor = function(speedFactor) {
	if (this._moving) {
		this.saveState();
	}

	this._speedFactor = speedFactor;
};

Move.prototype.saveState = function () {

	if (this._moveStart == -1) {
		alert("something is not quite right");
	}

	var moveStartDate = this._moveStart;
	var moveEndDate = new Date();

	this._moveStart = moveEndDate;
	var travelTime = moveEndDate - moveStartDate;

	var distance = 0;

	if(travelTime < this._lastNode.wait) {
		cosole.log("");
		this._lastNode.wait = this._lastNode.wait - travelTime;
		return;
	} else {
		travelTime = travelTime - this._lastNode.wait;
		this._lastNode.wait = 0;
		distance = this._lastNode.speed * MoveUtil.millisecondsToHours(travelTime, this._speedFactor);
	}

	for (var i = this._lastNodeIndex; i < this._nodes.length ; i++) {
		if (distance > this._lastNode.point.distanceTo(this._nodes[i + 1].point)) {
			var distanceToNode = distance - this._lastNode.point.distanceTo(this._nodes[i + 1].point);
			travelTime = travelTime - (distanceToNode / this._lastNode.speed);
			this._lastNodeIndex = i + 1;
			this._lastNode = this._nodes[i + 1];
			if(travelTime - this._lastNode.wait > 0) {
				travelTime = travelTime - this._lastNode.wait;
				distance = this._lastNode.speed * MoveUtil.millisecondsToHours(travelTime, this._speedFactor);
				this._lastNode.wait = 0;
			} else {
				this._lastNode.wait = this._lastNode.wait - travelTime;
				distance = 0;
			}
			if (i + 2 < this._nodes.length) {
				this._bearing = this._nodes[i + 1].point.bearingTo(this._nodes[i + 2].point);
			} else {
				this._lastNode = this._nodes[this._nodes.length - 1];
				this._moving = false;
				return;
			}
		} else {
			break;
		}
	}

	this._lastNode = { "point": this._lastNode.point.destinationPoint(this._bearing, distance), "wait": 0, "speed" : this._lastNode.speed};
};


MoveUtil = {};

MoveUtil.millisecondsToHours = function (milli, speedFactor) {
	return milli / 1000 / 60 / 60 * speedFactor;
};

MoveUtil.getLatLon = function (point) {
	return new LatLon(point[0], point[1]);
};


