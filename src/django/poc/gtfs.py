from geodjango.poc.models import Stops, Trips, Routes, Shapes
from geodjango.poc.dbviews import Edges2
from datetime import timedelta
from datetime import datetime
from django.utils import timezone
import sys
import logging
from threading import Lock
import random
from geodjango.poc.gtfsrouting import GtfsRouter, GtfsRoute, RoutingAlgo, Point, FOOT_TRIP_ID
from django.utils.timezone import utc
from django.conf import settings
import math

#from google_perftools_wrapped import StartProfiler, StopProfiler

sys.path.append("/opt/django-1.5.1/lib/python2.7/site-packages")
from geographiclib.geodesic import Geodesic

log = logging.getLogger(__name__)

RouteTypes = ["tram", "subway", "rail", "bus", "ferry", "cablecar", "gondola", "funicular", "foot"]

EARTH_RADIUS_IN_M = 6371000

def crossTrackDistance(startLat, startLon, endLat, endLon, thirdLat, thirdLon):
    startEndAzi = Geodesic.WGS84.Inverse(startLat, startLon, endLat, endLon)['azi1']
    startThirdAzi = Geodesic.WGS84.Inverse(startLat, startLon, thirdLat, thirdLon)['azi1']
    distStartThirt = distance((startLat, startLon), (thirdLat, thirdLon))
    return math.asin(math.sin(distStartThirt / EARTH_RADIUS_IN_M) * math.sin(startThirdAzi - startEndAzi)) * EARTH_RADIUS_IN_M

def alongTrackDistance(startLat, startLon, endLat, endLon, thirdLat, thirdLon):
    distStartThirt = distance((startLat, startLon), (thirdLat, thirdLon))
    ctd = crossTrackDistance(startLat, startLon, endLat, endLon, thirdLat, thirdLon)
    return math.acos(math.cos(distStartThirt / EARTH_RADIUS_IN_M )/ math.cos( ctd / EARTH_RADIUS_IN_M)) * EARTH_RADIUS_IN_M

def atdAndPoint(startLat, startLon, endLat, endLon, thirdLat, thirdLon):
    atd = alongTrackDistance(startLat, startLon, endLat, endLon, thirdLat, thirdLon)
    point = getPositionOnLine((startLat, startLon), (endLat, endLon), atd)
    return (atd, point)

def euclideanDistance(x, y):
    return math.sqrt( (110992*(x[0] - y[0]))**2 + (88090*(x[1] - y[1]))**2 )

def trimShape2TripPartSimple(shape, start, end):
    minStart = None
    minEnd = None
    for idx in range(0, len(shape)):
        startDist =  euclideanDistance(start, shape[idx])
        if minStart == None or minStart[0] > startDist:
            minStart = (startDist, idx)

    if minStart[1] + 1 == len(shape):
        log.error("Dirty hack on in it glory!!")
        return [start, shape[minStart[1]], end]

    for idx in range(minStart[1] + 1, len(shape)):
        endDist =  euclideanDistance(end, shape[idx])
        if minEnd == None or minEnd[0] > endDist:
            minEnd = (endDist, idx)

    assert(minStart[1] + 1 < len(shape))

    result = shape[minStart[1]: minEnd[1] + 1]
    result[0] = start
    result += [end]

    return result

def trimShape2TripPart(shape, location1, location2):
    minStart = None
    minEnd = None
    for idx in range(0, len(shape) - 1):
        path = (shape[idx], shape[idx + 1])
        sdist, spoint = atdAndPoint(path[0][0], path[0][1], path[1][0], path[1][1], location1[0], location1[1])
        startDist =  distance(location1, spoint)
        if minStart == None or minStart[0] > startDist:
            minStart = (startDist, spoint, idx)

        edist, epoint = atdAndPoint(path[0][0], path[0][1], path[1][0], path[1][1], location2[0], location2[1])
        endDist =  distance(location2, epoint)
        if minEnd == None or minEnd[0] > endDist:
            minEnd = (endDist, epoint, idx)

    #assert(minStart[2] <= minEnd[2])
    result = [minStart[1]]

    for idx in range(minStart[2] + 1, minEnd[2]):
        result += [shape[idx]]
    result = [minEnd[1]]
    return result

def getPositionOnLine(x, y, dist):
    values = Geodesic.WGS84.Inverse(x[0], x[1], y[0], y[1])
    line = Geodesic.WGS84.Line(values["lat1"], values["lon1"], values["azi1"])
    pos = line.Position(dist)
    return [pos["lat2"], pos["lon2"]]

def calculateDistance(shapes):
    dist = 0
    for i in range(0, len(shapes) - 1):
        dist += distance(shapes[i], shapes[i + 1])
    return dist

class UserTravel:
    def __init__(self, startTime, fromLocation, toLocation, gtfsRoute):
        self.travelId = self.__generateTravelId()
        self.time = gtfsRoute.time
        self.nodes = []
        self.startTime = startTime
        self.fromLocation = fromLocation
        self.toLocation = toLocation

        self.__transform(gtfsRoute, fromLocation, toLocation)

    def toDict(self):
        return { "totalTravelTime": self.time, "travelId": self.travelId, "nodes": self.nodes }

    def __transform(self, gtfsRoute, fromLocation, toLocation):
        log.info("Transform start")

        stopModels = Stops.objects.in_bulk([vertex.stopId for vertex in gtfsRoute.verticles])
        tripModels = Trips.objects.in_bulk([vertex.tripId for vertex in gtfsRoute.verticles])
        routeModels = Routes.objects.in_bulk([trip.route_id for trip in tripModels.values()])
        log.info("Bulk selects done")

        # TODO: handle no foot situation, handle last node
        for i in range(0, len(gtfsRoute.verticles)):
            stopPosition = None
            vertex = gtfsRoute.verticles[i]# TODO: is it random?
            node = {}
            if i == 0: # TODO: handle start from stop
                node["stopName"] = "startPoint"
                node["stopPosition"] = fromLocation
                node["waitTime"] = vertex.departureTime - self.startTime
                node["departureTime"] = vertex.departureTime
                node["travelTime"] = vertex.arrivalTime - vertex.departureTime
                node["innerStops"] = []
                node["tripId"] = 0
                node["line"] = "foot"
                node["type"] = "foot"
                node["headSign"] = ""
            elif vertex.tripId != gtfsRoute.verticles[i - 1].tripId:
                prevVertex = gtfsRoute.verticles[i - 1]
                if vertex.tripId != FOOT_TRIP_ID:
                    route = routeModels[tripModels[vertex.tripId].route_id]
                    node["type"] = RouteTypes[route.route_type]
                    node["_shapeId"] = tripModels[vertex.tripId].shape_id
                    node["tripId"] = vertex.tripId
                    node["line"] = route.route_short_name
                    node["headSign"] = tripModels[vertex.tripId].trip_headsign
                else:
                    node["tripId"] = 0
                    node["line"] = "foot"
                    node["type"] = "foot"
                    node["headSign"] = ""
                stop = stopModels[prevVertex.stopId]

                node["innerStops"] = []
                node["waitTime"] = vertex.departureTime - prevVertex.arrivalTime
                node["departureTime"] = vertex.departureTime
                node["travelTime"] = vertex.arrivalTime - vertex.departureTime
                node["stopPosition"] = (float(stop.stop_lat), float(stop.stop_lon))
                node["stopName"] = stop.stop_name
            else:
                self.nodes[-1]["travelTime"] += vertex.arrivalTime - vertex.departureTime
                stopId = gtfsRoute.verticles[i - 1].stopId
                position = ( float(stopModels[stopId].stop_lat), float(stopModels[stopId].stop_lon) )
                self.nodes[-1]["innerStops"] += [ { "stopId": stopId, "position": position } ] # TODO: one query
                continue

            self.nodes += [node]

        log.info("Shape transform")
        for i in range(0, len(self.nodes)):
            node = self.nodes[i]
            if i == 0:# and node["line"] == "foot":
                node["shape"] = [ fromLocation ]
                continue
            if node["line"] == "foot":
                assert(i == len(self.nodes) - 1)
                #self.nodes[i - 1]["shape"] += [ node["stopPosition"] ]
                node["shape"] = [ node["stopPosition"], toLocation ]
                break
            assert(node["line"] != "foot")
            
            if i == len(self.nodes) - 1:
                nexStop = self.nodes[-1]["stopPosition"]
            else:
                nextStop = self.nodes[i + 1]["stopPosition"]

            shape = [ (float(s.shape_pt_lat), float(s.shape_pt_lon)) for s in Shapes.objects.filter(shape_id = node["_shapeId"]) ]
            del node["_shapeId"]

            node["shape"] = trimShape2TripPartSimple(shape, node["stopPosition"], nextStop)
            self.nodes[i - 1]["shape"] += [ node["shape"][0] ]

        log.info("Transform end")

    def __generateTravelId(self):
        return "%08x" % random.randrange(16**8)

class UserTravels2:
    def __init__(self):
        self.travels = {}
        self.mutex = Lock()

    def get(self, travelId):
        with self.mutex:
            return self.travels.get(travelId)

    def put(self, travel):
        with self.mutex:
            self.travel[travel.travelId] = travel


    def calculatePositionAtTime(time):
        assert(time >= self.startTime)

        # didn't start yet
        if time <= self.startTime + self.nodes["waitTime"]:
            return self.fromLocation

        if time == self.nodes[-1]["departureTime"] + self.nodes[-1]["travelTime"]:
            return self.toLocation

        # The trip is over
        if time > self.nodes[-1]["departureTime"] + self.nodes[-1]["travelTime"]:
            return None

        idx = bisect_left([node["departureTime"] for node in self.nodes], time)
        node[idx] = self.nodes[idx]

        if time >= node["departureTime"] + node["travelTime"]:
                return self.nodes[idx + 1]["stopPosition"]

        speed = self.calculateDistance(self.shapes[idx]) / node[idx]["travelTime"]
        metersPassed = (time - node[idx]["departureTime"]) * speed

        for idx in range(0, len(self.shapes) - 1):
            dist = distance(self.shapes[idx], self.shapes[idx + 1])
            if dist < metersPassed:
                metersPassed -= dist
            else:
                return getPositionOnLine(self.shapes[idx], self.shapes[idx + 1], metersPassed)

    def __generateTravelId(self):
        return "%08x" % random.randrange(16**8)

class UserTravels:
    def __init__(self):
        self.travels = {}
        self.mutex = Lock()

    def get(self, travelId):
        with self.mutex:
            return self.travels.get(travelId)

    def put(self, travel):
        with self.mutex:
            self.travels[travel.travelId] = travel


userTravels = UserTravels()

def distance(x, y):
    return Geodesic.WGS84.Inverse(x[0], x[1], y[0], y[1])["s12"]

def getWalkTimeFromTo(start, end):
    return int(distance(start, end) / (5000.0 / 3600))

def getNearStops(position, num):
    distances = []
    for stop in Stops.objects.all():
        stopPosition = (float(stop.stop_lat), float(stop.stop_lon))
        distances.append((distance(stopPosition, position), stop.stop_name, stopPosition) )

    return [ {"name": xtuple[1], "position": xtuple[2]} for xtuple in sorted(distances, key = lambda xtuple : xtuple[0])[:num] ]

########### protocol calls ###############

def logSearchSummary(routes):
    log.info("\tSummary of %s results" % len(routes))
    for route in routes:
        log.info("\t\t time: %s, verticles: %s" % (route.time, len(route.verticles)))


def findroute(fromLocation, toLocation):

    now = datetime.utcnow().replace(tzinfo=utc)
    #now = utc.now()
    log.info("now hour %s min %s " % (now.hour, now.minute))
    startTime = now.hour * 3600 + now.minute * 60 + now.second
    log.info("now is %s (%s)" % (now, startTime))
    if now.weekday() < 5:
        serviceId = 1
    elif now.weekday() == 5:
        serviceId = 2
    else:
        serviceId = 3
    #serviceId = 1

    fromPoint = Point(fromLocation[0], fromLocation[1])
    toPoint = Point(toLocation[0], toLocation[1])

    numRoutesToReturn = 3
    numStartStops = 40
    numEndStops = 40
    minChangeTime = 0
    maxChangeTime = 60*15
    routes = GtfsRouter(RoutingAlgo.ASTAR, fromPoint, toPoint, startTime,
                        numRoutesToReturn, numStartStops, numEndStops,
                        minChangeTime, maxChangeTime, serviceId).route()
    logSearchSummary(routes)

    travelList = []
    for gtfsRoute in routes:
        if gtfsRoute.verticles:
            travel = UserTravel(startTime, fromLocation, toLocation, gtfsRoute)
            if len(travel.nodes) > 1:
                log.info("Travel 1st vehicle <Line: %s Stop: %s, StartTime: %s>"
                    % (travel.nodes[1]["line"], travel.nodes[1]["stopName"], travel.nodes[1]["departureTime"]))
            #userTravels.put(travel)
            travelList += [ travel.toDict() ]
    return travelList 

def getTravelStatus(travelId, position):
    trip = userTravels.get(travelId)
    return "OK"
