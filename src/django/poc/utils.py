
class AssertException(Exception):
    pass

def RAISE_IF(cond, comment = None):
    if cond:
        if comment:
            raise AssertException, comment
        else:
            raise AssertException

