from geodjango.poc.models import Routes
from django.http import HttpResponse
import django.utils.simplejson as json
from django.utils import timezone
import logging
from threading import Lock
from geodjango.poc.nextBus import NextBusReader
from geodjango.poc import gtfs
import random
from copy import deepcopy

mutex = Lock()

def copyClosestVehicles(reports, fromLocation, num):
    distances = set()
    for idx in range(0, len(reports)):
        distances.add( (gtfs.distance(fromLocation, reports[idx]["position"]), idx) )

    return [ deepcopy(reports[xtuple[1]]) for xtuple in sorted(distances, key = lambda xtuple : xtuple[0])[:num] ]

log = logging.getLogger(__name__)

nextBusReader = NextBusReader()
nextBusReader.start()
vehiclesReports = []

def findroute(request):
    data = json.loads(request.body)
    log.info("########## FINDROUTE: %s -> %s (IP %s) ##########" % (data["from"], data["to"], request.META["REMOTE_ADDR"]))

    start = timezone.now()
    result = gtfs.findroute(data["from"], data["to"])

    log.info("########## PROCESSING TIME: %s ###########" % (timezone.now() - start))
    return HttpResponse(json.dumps(result))

def reportpos(request):
    global vehiclesReports
    data = json.loads(request.body)

    travelId = data["travelId"]
    nearVehicles = []
    nearStops = []
    #if not travelId: # initial request
    #    with mutex:
    #        nearVehicles = findClosestVehicles(vehiclesReports, data["position"], 20)
    #        nearStops = gtfs.getNearStops(data["position"], 20)
    with mutex:
        nearVehicles = copyClosestVehicles(vehiclesReports, data["position"], 20)

    log.info("reportpos: returning report of %s bus " % (len(nearVehicles),))
    routeModels = Routes.objects.filter(route_short_name__in = [vehicle["line"] for vehicle in nearVehicles])

    routeModelsDict = { item["route_short_name"]: item["route_type"] for item in routeModels.values() }
    for vehicle in nearVehicles:
        if vehicle["line"] in routeModelsDict:
            vehicle["type"] = gtfs.RouteTypes[routeModelsDict[vehicle["line"]]]
        else:
            log.info("LINE NOT FOUND %s" % (vehicle,))
            vehicle["type"] = gtfs.RouteTypes[3] # bus

    reply = {
        "travelId": travelId,
        "nearStops": [],#nearStops,
        "nearVehicles": nearVehicles,
        "status": "OK",
    }

    return HttpResponse(json.dumps(reply))

#TODO: proper vehicle types
def nextbusreport(request):
    global vehiclesReports
    reports = json.loads(request.body)
    if len(reports) == 0:
        return
    with mutex:
        vehiclesReports = []
        for report in reports:
            vehiclesReports.append(report)
    return HttpResponse()

