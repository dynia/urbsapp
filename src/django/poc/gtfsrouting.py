from geodjango.poc.models import Stops
from geodjango.poc.dbviews import Edges2
import heapq
import sys
import copy
import math
from bisect import bisect_left, bisect_right

sys.path.append("/opt/django-1.5.1/lib/python2.7/site-packages")
from geographiclib.geodesic import Geodesic

def distance(lat1, lon1, lat2, lon2):
    return int(Geodesic.WGS84.Inverse(lat1, lon1, lat2, lon2)["s12"])

def distance3(lat1, lon1, lat2, lon2):
    return math.sqrt(math.pow((lat1 - lat2) * 111, 2) + math.pow((lon1 - lon2) * 70, 2)) * 1000;

def timeFromTo(lat1, lon1, lat2, lon2):
    return int(distance3(lat1, lon1, lat2, lon2) / 32)

class GtfsVertex:
    def __init__(self, id, tripId, departureTime, arrivalTime):
        self.id = id
        self.stop = None
        self.tripId = tripId
        self.departureTime = int(departureTime)
        self.arrivalTime = int(arrivalTime)

    def getNeighbourVerticles(self):
        global globalData
        result = []
        stop = globalData.allStops[self.stop]
        lowerBoundIdx = bisect_left([ pair[0] for pair in stop.edges ], self.arrivalTime)

        for departure, vertexId in stop.edges[lowerBoundIdx:]: # TODO: list comprehension?
            result += [vertexId]
        return result

    def __hash__(self):
        return self.id 

    def __eq__(self, other):
        return self.id == other.id

class StopVertex:
    def __init__(self, id, lat, lon):
        self.stopId = id
        self.lat = lat
        self.lon = lon 
        self.edges = []

    def __hash__(self):
        return self.id 

    def __eq__(self, other):
        return self.id == other.id

class GlobalData:
    def __init__(self):

        self.allVerticles = {}
        self.allStops = {}

        for edge in Edges2.objects.all().order_by('-id'):
            vertex = GtfsVertex(edge.id, edge.trip_id, edge.departure_timestamp, edge.arrival_timestamp) 
            self.allVerticles[edge.id] = vertex

            if edge.next_stop_id not in self.allStops:
                stopModel = Stops.objects.get(stop_id = edge.next_stop_id)
                self.allStops[edge.next_stop_id] = StopVertex(stopModel.stop_id, stopModel.stop_lat, stopModel.stop_lon)

            self.allVerticles[edge.id].stop = edge.next_stop_id

            if edge.stop_id not in self.allStops:
                stopModel = Stops.objects.get(stop_id = edge.stop_id)
                self.allStops[edge.stop_id] = StopVertex(stopModel.stop_id, stopModel.stop_lat, stopModel.stop_lon)

            self.allStops[edge.stop_id].edges += [ (vertex.departureTime, vertex.id) ] # Python doesn't have multiset :/

        for stop in self.allStops.values():
            stop.edges.sort()

globalData = GlobalData()

class RouteNodesImpl:
    def __init__(self, time, estimate):
        self.time = time
        self.estimate = estimate
        self.verticles = []

ASTAR = 65
DIJKSTRA = 68

class GtfsRouter: 
    def __init__(self, algo, tripStartTime, numClosestStops, maxLoops):
        self.algo = algo
        self.tripStartTime = tripStartTime
        self.numClosestStops = maxLoops
        self.maxLoops = maxLoops
        self.startStop = None
        self.endStop = None
        self.open = {}
        self.closed = {}
        self.routes = []
        heapq.heapify(self.routes)
 
    def route(self, startPoint, endPoint):
        startClosest = (0, None)
        endClosest = (0, None)

        global globalData
        for stop in globalData.allStops.values():
            startDist = distance(startPoint[0], startPoint[1], float(stop.lat), float(stop.lon))  
            endDist = distance(endPoint[0], endPoint[1], float(stop.lat), float(stop.lon))
 
            if startClosest[1] == None or startClosest[0] > startDist:
                startClosest = (startDist, stop.stopId)
            if endClosest[1] == None or endClosest[0] > endDist:
                endClosest = (endDist, stop.stopId)
        self.startStop = startClosest[1]
        self.endStop = endClosest[1]

        print " routing using %c from %s , %sm, to %s, %sm" % (self.algo, self.startStop, startClosest[0], self.endStop, endClosest[0])
        return self.routeImpl()

    def routeImpl(self):
        startVertex = GtfsVertex(0, 88888, self.tripStartTime, self.tripStartTime + 100)
        startVertex.stop = self.startStop

        initialRoute = RouteNodesImpl(0, 0)
        initialRoute.verticles += [startVertex.id] 
        self.open[self.startStop] = 0
        heapq.heappush(self.routes, (0, initialRoute));

        for i in range(0, self.maxLoops):
            try:
                timeAndEstimate, route = heapq.heappop(self.routes)
            except IndexError:
                print "No Route, count %s" % i
                return 54321

            vertexId = route.verticles[-1]
            if vertexId == 0:
                vertex = startVertex
            else:
                vertex = globalData.allVerticles[vertexId]

            if vertex.stop not in self.open or self.open[vertex.stop] < route.time:
                continue
            assert(self.open[vertex.stop] == route.time);
            
            self.closed[vertex.stop] = route.time
            del self.open[vertex.stop]

            if vertex.stop == self.endStop:
                print " Success in %s counts,verticles: %s" % (i, len(route.verticles))
                return route.time

            for nextId in vertex.getNeighbourVerticles():
                self.addToQueue(vertex, nextId, route) 

        return RouteNodesImpl(0, 0).time

    def addToQueue(self, vertex, nextVertexId, route):
        global globalData


        nextVertex = globalData.allVerticles[nextVertexId]
        time = nextVertex.arrivalTime - self.tripStartTime

        if nextVertex.stop in self.closed and time >= self.closed[nextVertex.stop]:
            return  
        if nextVertex.stop in self.open and time >= self.open[nextVertex.stop]:
            return

        end = globalData.allStops[self.endStop]
        nextStop = globalData.allStops[nextVertex.stop]

        estimate = 0
        if self.algo == ASTAR:
           estimate = timeFromTo(nextStop.lat, nextStop.lon, end.lat, end.lon)
 
        newRoute = RouteNodesImpl(time, estimate) 
        newRoute.verticles = copy.deepcopy(route.verticles)
        newRoute.verticles += [nextVertexId]

        self.open[nextVertex.stop] = newRoute.time

        heapq.heappush(self.routes, (newRoute.time + newRoute.estimate, newRoute));

