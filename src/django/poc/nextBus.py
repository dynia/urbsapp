import threading
import os
import json
import time
import httplib
import re
import logging

log = logging.getLogger(__name__)

class NextBusReader(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.nextBusHost = "webservices.nextbus.com"
        self.baseUrl = "/service/publicXMLFeed?"
        self.agency = "a=sf-muni"
        self.command = "command=vehicleLocations"
        self.lastGetTime = "t=0"
        self.reports = []
        self.period = 12
        self.vehicleRegex = re.compile(r'\s*<vehicle id="(.+)" routeTag="(.+)" dirTag="(.+)" lat="([-+]?\d*\.\d+|\d+)" lon="([-+]?\d*\.\d+|\d+)".*')
        self.timeRegex = re.compile(r'<lastTime time="(.+)"/>')

        log.info("NextBusReader inited")

    def getPositions(self):
        try:
            conn = httplib.HTTPConnection(self.nextBusHost)
            conn.request("GET", self.baseUrl + self.agency + "&" + self.command + "&" + self.lastGetTime)
            resp = conn.getresponse()
            self.reports = []

            data = resp.read()

            numReports = 0
            for line in data.splitlines():
                vehicleLine = self.vehicleRegex.match(line)
                if vehicleLine:
                    numReports += 1
                    self.reports += [{"id": str(vehicleLine.group(1)), "line": str(vehicleLine.group(2)),
                                    "position": [float(vehicleLine.group(4)), float(vehicleLine.group(5))]}]
                else:
                    timeLine = self.timeRegex.match(line)
                    if timeLine:
                        self.lastGetTime = "t=%s" % timeLine.group(1)

            log.info("NextBus.com data len = %s, num vehicles = %s" % (len(data), numReports))
        except Exception, ex:
            log.info("Connection Exeception %s" % ex)

    def reportPositions(self):
        try:
            conn = httplib.HTTPConnection("127.0.0.1")
            conn = conn.request("GET", "/proto/nextbusreport/", json.dumps(self.reports))
        except Exception, ex:
            log.info("Connection Exeception %s" % ex)

    def run(self):
        log.info("NextBusReader starting...")
        while True:
            log.info("Getting data from NextBus.com")
            self.getPositions()
            self.reportPositions()
            time.sleep(self.period)
        log.info("NextBusReader stopped")

if __name__ == "__main__":
    reader = NextBusReader()
    #reader.setDaemon(True)
    reader.run()
