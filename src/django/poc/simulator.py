import json
import time
import math
import threading
import sys
import httplib
import random
import getopt
import logging
import os
import copy

from bisect import bisect_left, bisect_right
sys.path.append("/opt/django-1.5.1/lib/python2.7/site-packages")
from geographiclib.geodesic import Geodesic

log = logging.getLogger(__name__)

class AssertException(Exception):
    pass 

def RAISE_IF(cond, comment = None):
    if cond:
        if comment:
            raise AssertException, comment
        else:
            raise AssertException

def distance(x, y):
    return Geodesic.WGS84.Inverse(x[0], x[1], y[0], y[1])["s12"]

#TODO: dirty hack for walk scaling

walkScale = 1

def getWalkTimeFromTo(start, end):
    global walkScale
    return distance(start, end) / (walkScale * 5000 / 3600.0)

def getPositionOnLine(x, y, dist):
    values = Geodesic.WGS84.Inverse(x[0], x[1], y[0], y[1])
    line = Geodesic.WGS84.Line(values["lat1"], values["lon1"], values["azi1"])
    pos = line.Position(dist)
    return [pos["lat2"], pos["lon2"]]

class LineInfo:
    def __init__(self, line, geometry, nodes, allStops, reverseDirection, randomize):
        RAISE_IF(geometry not in ("circle", "line"), "Wrong line geometry %s for line %s" % (geometry, line))
        RAISE_IF(len(nodes) < 2, "To few nodes for line %s" % line)

        self.line = line

        self.geometry = geometry
        self.nodes = copy.deepcopy(nodes) 
        if reverseDirection:
            self.nodes.reverse()

        self.nodes2stops = []

        self.__adjustNodesToStops(allStops)

        if not reverseDirection:
            self.line += "A"
        else:
            self.line += "B"

        self.randomize = randomize
        self.wholeRouteDistance = self.distanceBetweenNodes(self.nodes[0], self.nodes[-1])

        log.info("LINE %s: geometry: %s, nodes %s, stops %s, distance %s" %
            (self.line, self.geometry, len(self.nodes), len(self.nodes2stops), self.wholeRouteDistance))

    def __adjustNodesToStops(self, allStops):
        log.info("Adjusting nodes to stops for line %s" % self.line)
        myStopsNum = 0
        for stop in allStops:
            if self.line in stop["lines"]:
                myStopsNum += 1
                closestId = 0
                closestDistance = distance(self.nodes[closestId], stop["position"])
                for i in range(1, len(self.nodes)):
                    dist = distance(self.nodes[i], stop["position"])
                    if dist < closestDistance:
                        #log.info("New closest to %s: %s with dist %s, so far was %s with dist %s " % (stop["id"], i, dist, closestId, closestDistance))
                        closestId = i
                        closestDistance = dist

                self.nodes2stops.append((closestId, stop))
                self.nodes[closestId] = stop["position"]

        assert(myStopsNum > 0)
        # check if every stop had _unique_ match
        RAISE_IF(myStopsNum != len(set(pair[0] for pair in self.nodes2stops)), "myStopsNum: %s, mapping: %s" % (myStopsNum, self.nodes2stops))

        self.nodes2stops = sorted(self.nodes2stops, key = lambda pair : pair[0])

        # for "line" geometry stops cannot be both first and last nodes (to avoid teleportation between stops)
        assert(self.geometry != "line" or (self.nodes2stops[0][0] != 0 or self.nodes2stops[-1][0] != len(self.nodes) - 1))

    def getStartPosition(self):
        startNode = 0
        if self.randomize:
            startNode = random.randrange(0, len(self.nodes) - 1)

        return (self.nodes[startNode], self.nodes[startNode + 1])

    # TODO: I guess similar code is there somewere 
    def distanceBetweenNodes(self, sourceNode, destinationNode):
        RAISE_IF((sourceNode not in self.nodes) or (destinationNode not in self.nodes),
                "src: %s, dst: %s [%s, nodes: %s]" % (sourceNode, destinationNode, self, self.nodes))
 
        dist = 0
        currentNodeId = self.nodes.index(sourceNode)
        nextNodeId = None

        while self.nodes[currentNodeId] != destinationNode:
            nextNodeId = (currentNodeId + 1) % len(self.nodes)

            if nextNodeId == 0 and self.geometry == "line":
                currentNodeId = 0
                nextNodeId = 1
                continue

            ##log.info("dist: %s " % (dist,))
            dist += distance(self.nodes[currentNodeId], self.nodes[nextNodeId])
            currentNodeId = nextNodeId

        return dist

    # TODO: return empty on line geometry for drawing
    def findNextStopFromNode(self, fromNode, bisect_function, wrapAround):
        RAISE_IF((fromNode not in self.nodes), "fromNode: %s, line: %s, nodes: %s" % (fromNode, self.line, self.nodes))

        i = bisect_function([ pair[0] for pair in self.nodes2stops ], self.nodes.index(fromNode))
        if i < len(self.nodes2stops):
                return self.nodes2stops[i][1]
        elif wrapAround:
                return self.findNextStopFromNode(self.nodes[0], bisect_function, False)
        else:
            return None

    def getNextNodeForPosition(self, currentPosition, currentNextNode):
        position = currentNextNode
        nextNodeId = (self.nodes.index(currentNextNode) + 1) % len(self.nodes)

        if nextNodeId == 0 and self.geometry == "line":
            nextNodeId = 1
            position = self.nodes[0]

        return position, self.nodes[nextNodeId]
    
    def __str__(self):
        return self.__unicode__()

    def __unicode__(self):
        return "InfoLine: line:%s"

class Vehicle(threading.Thread):
    def __init__(self, lineInfo, conf):
        threading.Thread.__init__(self)
        assert(len(conf["nodes"]) > 1)
        self.lineInfo = lineInfo
        self.mutex = threading.Lock()
        self.report_host = conf["report_host"]
        self.report_url = conf["report_url"]
        self.id = lineInfo.line #TODO: proper id
        self.type = conf["type"]
        self.speed = conf["speed"]
        self.period = conf["report_period"]
        self.geometry = conf["geometry"]
        self.progres = ((self.speed * 1000.0) / 3600) * self.period
        self.position, self.nextNode = self.lineInfo.getStartPosition()

    def __unicode__(self):
        return u"%s: %s, %s km/h, period: %s sec, progres %s m" % (self.type, self.lineInfo.line, self.speed, self.period, self.progres)

    def __str__(self):
        return self.__unicode__()

    def __go(self):
        with self.mutex:
            to_go = self.progres
            while to_go >= distance(self.position, self.nextNode):
                to_go -= distance(self.position, self.nextNode)
                assert(to_go >= 0)
                self.position, self.nextNode = self.lineInfo.getNextNodeForPosition(self.position, self.nextNode)

            if to_go:
                self.position = getPositionOnLine(self.position, self.nextNode, to_go)

    def run(self):
        log.info("Starting vehicle: %s from %s, reporting addrs: %s%s" % (self, self.position, self.report_host, self.report_url))
        while True:
            stopPosition = None
            nextStop = self.lineInfo.findNextStopFromNode(self.nextNode, bisect_left, "line" != self.lineInfo.geometry)
            if nextStop:
                stopPosition = nextStop["position"]

            report = {"id": self.id, "line": self.lineInfo.line, "position": self.position, "type": self.type, "nextstop" : stopPosition}
            try:
                conn = httplib.HTTPConnection(self.report_host)
                conn = conn.request("GET", self.report_url, json.dumps(report))
            except Exception, ex:
                log.info("Connection Exeception %s" % ex)
            time.sleep(self.period)
            self.__go()

    def calculateDistanceTo(self, destination):
        with self.mutex:
            return distance(self.position, self.nextNode) + self.lineInfo.distanceBetweenNodes(self.nextNode, destination)

    def calculateTimeTo(self, destination):
        return self.calculateDistanceTo(destination) / ((self.speed * 1000) / 3600.0)

    def calculateTimeFromTo(self, source, destination):
        return self.lineInfo.distanceBetweenNodes(source, destination) / ((self.speed * 1000) / 3600.0)

    def getWholeRouteTravelTime(self):
        return self.lineInfo.wholeRouteDistance / ((self.speed * 1000) / 3600.0)

def createVertexFromStop(stop, line):
    vertex = copy.deepcopy(stop)
    vertex["line"] = line
    vertex["edges"] = []
    return vertex 

def printGraph(graph):
    log.info("******** GRAPH **********")
    totalEdges = 0

    for vertexKey, vertexValues in graph.items():
        log.info("\t %s" % (vertexKey,))
        for neighbourVertex in vertexValues["edges"]:
            totalEdges += 1
            log.info("\t\t(%s, %s)" % (neighbourVertex["id"], neighbourVertex["line"]))

    log.info("verticles: %s, endges: %s" % (len(graph), totalEdges))

def getRouteAsNodes(delay, verticles):
    log.info("converting nodes to route")
    return {"delay": delay, "stops": [[vertex["line"], vertex["position"], vertex["id"], vertex["name"]] for vertex in verticles] }

def getRouteWithDescription(items):
    assert(len(items["waitTimes"]) == len(items["rideTimes"]))
    assert(len(items["edges"]) == len(items["rideTimes"]) + 1)

    nodes = []
    verticles = items["edges"]

    for i in range(0, len(verticles) - 1):
        vertex = verticles[i]
        nextVertex = verticles[i + 1]
        log.info("ROUTE: %s %s" % (vertex["name"], vertex["line"]))

        if vertex["line"] == "feet" or nextVertex["line"] == "feet":
            description = "Walk from %s to %s" % (vertex["name"], nextVertex["name"])
        elif vertex["line"] == nextVertex["line"]:
            description = "Keep riding %s on %s to get to %s" % (vertex["line"], vertex["name"], nextVertex["name"])
        else:
            description = "Take %s from %s to get to %s" % (nextVertex["line"], vertex["name"], nextVertex["name"])

        nodes += [{"rideTime": items["rideTimes"][i],
                    "waitTime": items["waitTimes"][i],
                    "description": description,
                    "position": vertex["position"],
                    "vehicleId": items["vehiclesIds"][i]
                    }]

    return nodes

class TrafficSimulator:
    def __init__(self):
        self.running = False
        self.vehicles = {}
        self.lineInfos = {}
        self.graph = {}
        self.mutex = threading.Lock()
        self.queue = []

        with open(os.path.join(os.path.dirname(__file__), 'sf.conf'), "r") as f:
            self.config = json.loads(f.read())

        global walkScale
        walkScale = walkScale * self.config["speed_scale"]

        for vehicleConf in self.config["vehicles"]:
            line = vehicleConf["line"]
            geometry = vehicleConf["geometry"]
            nodes = vehicleConf["nodes"]
            randomize = self.config["randomize"]
            allStops = self.config["allstops"]

            vehicleConf["report_host"] = self.config["report_host"]
            vehicleConf["report_url"] = self.config["report_url"]
            vehicleConf["report_period"] = self.config["report_period"]
            vehicleConf["speed"] = vehicleConf["speed"] * self.config["speed_scale"]

            lineInfoA = LineInfo(line, geometry, nodes, allStops, False, randomize)
            vehicleA = Vehicle(lineInfoA, vehicleConf)
            self.vehicles[lineInfoA.line] = vehicleA # TODO: by id, not by line

            lineInfoB = LineInfo(line, geometry, nodes, allStops, True, randomize)
            vehicleB = Vehicle(lineInfoB, vehicleConf)
            self.vehicles[lineInfoB.line] = vehicleB # TODO: by id, not by line

            self.lineInfos[lineInfoA.line] = lineInfoA
            self.lineInfos[lineInfoB.line] = lineInfoB

        self.buildGraph()

        for vehicle in self.vehicles.values():
            vehicle.start()

        log.info("Simulator created: %s" % self)

    def getStopsFromConfig(self):
        return self.config["allstops"]

    def getRoutesFromConfig(self):
        return self.config["vehicles"]

    def getRoutesAdjastedToStops(self):
        routes = []
        with self.mutex:
            for vehicle in self.vehicles.values():
                route = { "id": vehicle.id, "type": vehicle.type, "line": vehicle.line, "speed": 40, "report_period": 3, "geometry": "line",
                        "nodes": vehicle.nodes
                }
                routes.append(route)
        return routes

    def findClosestStop(self, location):
        #log.info("Searching closest stop to location: %s" % location)
        closestStop = self.config["allstops"][0]
        closestDist = distance(closestStop["position"], location)

        for stop in self.config["allstops"]:
            dist = distance(location, stop["position"])
            if dist < closestDist:
                #log.info("Found new closest stop: (%sm) %s, instead of: (%sm) %s," % (closestDist, stop, dist, closestStop))
                closestStop = stop
                closestDist = dist

        return closestStop
    
    def buildGraph(self):
        for stop in self.config["allstops"]:
            for line in stop["lines"]:
                self.graph[(stop["id"], line + "A")] = createVertexFromStop(stop, line + "A")
                self.graph[(stop["id"], line + "B")] = createVertexFromStop(stop, line + "B")

        for vertexKey, vertexValues in self.graph.items():
            for neighbourLine in vertexValues["lines"]:
                # TODO: problem: stops from top of the map should not be neighbours of the ones from bottom
                for neighbour in (neighbourLine + "A", neighbourLine + "B"):
                    wrapAround = self.lineInfos[neighbour].geometry == "cirle"
                    nextStop = self.lineInfos[neighbour].findNextStopFromNode(vertexValues["position"], bisect_right, wrapAround)
                    if nextStop != None:
                        RAISE_IF(nextStop["id"] == vertexKey[0], "stop: %s line: %s neighbour: %s" % (nextStop["id"], vertexValues["line"], neighbour))
                        vertexValues["edges"] += [ self.graph[(nextStop["id"], neighbour)] ]

        printGraph(self.graph)

    def findNClosestStops(self, fromLocation, numClosest):
        stopsWithDistances = []

        for stop in self.config["allstops"]:
            stopsWithDistances += [ (stop, distance(fromLocation, stop["position"])) ]

        sortedList = sorted(stopsWithDistances, key = lambda pair : pair[1])
        return [stop for stop, dist in sortedList[:numClosest]]

    def findRoute(self, fromLocation, toLocation):
        log.info("********************************")
        log.info("Searching for quickest route from: %s to %s" % (fromLocation, toLocation))

        #startStop = self.findClosestStop(fromLocation)

        allRoutes = []
        for stop in self.findNClosestStops(fromLocation, 1):
        #for stop in self.config["allstops"]:
            allRoutes += [ self.doRouteForGivenStart(stop, toLocation, fromLocation) ]

        log.info("Using %s posible routes" % len(allRoutes))

        minTime = None
        bestRoute = None
        for route in allRoutes:
            log.info("Route: %s" % route)
            if route and (bestRoute == None or route["time"] < minTime):
                bestRoute = route["nodes"]
                minTime = route["time"]

        log.info("minTime  : %s" % minTime)
        log.info("bestRoute: %s" % bestRoute)
        return bestRoute

    def doRouteForGivenStart(self, startStop, toLocation, fromLocation):
        self.graphCopy = copy.deepcopy(self.graph)

        endStop = self.findClosestStop(toLocation)

        startVertex = None
        endVertex = {"id": "walk", "name": "end point", "position": toLocation, "lines": [], "edges" : [], "line": "feet"}

        for vertexKey, vertexValues in self.graphCopy.items():
            # only one iteration is enought,
            # all posible vehicle starting from startStop will be taken it this calculation
            # since for every such line its next stop is a neighbour of startVertex
            if startVertex == None and vertexKey[0] == startStop["id"]:
                startVertex = vertexValues
            elif vertexKey[0] == endStop["id"]:
                vertexValues["edges"] += [endVertex]

        return self.doRoute(fromLocation, startVertex, endVertex)

    def doRoute(self, fromLocation, startVertex, endVertex):
        closed = [startVertex]
        self.queue = []

        walkTime = getWalkTimeFromTo(fromLocation, startVertex["position"])
        startPointVertex = {"position": fromLocation, "vehicleId": "walk", "line": "feet", "id": "walk",  "name": "start point"}

        startItem = {"time": walkTime, "end": None, "waitTimes": [0], "vehiclesIds": ["walk"], "rideTimes": [walkTime],
                    "edges": [startPointVertex, startVertex]}

        for neighbourVertex in startVertex["edges"]:
            self.addToQueue(startVertex, neighbourVertex, startItem, True)

        routeNodes = {}

        count = 0
        while count < len(self.graphCopy)**2:
            count += 1
            try:
                nextItem = self.queue.pop(0)  
            except IndexError:
                log.info("No Route")
                return routeNodes

            item = nextItem["end"]
            if item in closed:
                continue
            if item == endVertex:
                log.info("Success!!! achieved after %s iterations" % count)
                for v in nextItem["edges"]:
                    log.info("Edge: line: %s stop: %s" % (v["line"], v["name"]))

                routeNodes["nodes"] = getRouteWithDescription(nextItem)
                routeNodes["time"] = nextItem["time"]
                return routeNodes

            closed.append(item)
            for vertex in item["edges"]:
                if vertex not in closed:
                    self.addToQueue(item, vertex, nextItem, False)

        log.info("Nothing found")
        return routeNodes

    def addToQueue(self, start, end, queueSoFar, startStop):
        for item in self.queue:
            if item["end"] == end:
               return

        if end["id"] == "walk":
            waitTime = 0
            rideTime = getWalkTimeFromTo(start["position"], end["position"])
            vehicleId = "walk"
        else:
            if not startStop and start["line"] == end["line"]:
                waitTime = 0
            else:
                waitTime = self.vehicles[end["line"]].calculateTimeTo(start["position"]) - queueSoFar["time"]
                goneBy = waitTime
                loop = 1
                while waitTime < 0: # vehicles already left, don't be sad, there's always another
                    waitTime = self.vehicles[end["line"]].getWholeRouteTravelTime() * loop + goneBy
                    loop += 1
                    assert(loop < 100)
            assert(waitTime >= 0)

            rideTime = self.vehicles[end["line"]].calculateTimeFromTo(start["position"], end["position"])
            vehicleId = self.vehicles[end["line"]].id

        queueItem = {\
            "time": queueSoFar["time"] + waitTime + rideTime,
            "rideTimes": queueSoFar["rideTimes"] + [rideTime],
            "waitTimes": queueSoFar["waitTimes"] + [waitTime],
            "vehiclesIds": queueSoFar["vehiclesIds"] + [vehicleId],
            "edges": queueSoFar["edges"] + [end],
            "end": end}

        count = 0
        for item in self.queue:
            if item["time"] > queueItem["time"]:
                self.queue.insert(count, queueItem)
                break
            count += 1
        else:
            self.queue.append(queueItem)

