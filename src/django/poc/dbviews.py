from django.db import models
from geodjango.poc.models import Trips
from django.db import models

class Edges2(models.Model):
    id = models.IntegerField(primary_key = True)
    trip_id = models.IntegerField()
    stop_id = models.IntegerField()
    departure_timestamp = models.IntegerField(null = False, blank = False)
    next_stop_id = models.IntegerField()
    arrival_timestamp = models.IntegerField(null = False, blank = False)
    travel_time = models.IntegerField(null = False, blank = False)

