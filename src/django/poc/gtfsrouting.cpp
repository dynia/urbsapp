#include "gtfsrouting.h"
#include <sys/syscall.h>
#include <sys/types.h>
#include <math.h>
#include <ctime>
#include <limits>

#include <boost/function.hpp>
#include <boost/noncopyable.hpp>
#include <boost/foreach.hpp>
#include <boost/thread/condition.hpp>
#include <boost/ptr_container/ptr_list.hpp>
#include <boost/asio.hpp>
#include <boost/optional.hpp>

//#include <assert.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <GeographicLib/Geodesic.hpp>
#include <fstream>

using namespace std;
using namespace boost;
using namespace GeographicLib;
namespace py = boost::python;

double const EARTH_RADIUS_IN_M = 6371000;
uint32 RVEC_MAX = 64;
static uint32 const FIRST_STOP_ID = 5350336;
static uint32 const INVALID_TRIP_ID = std::numeric_limits<uint32>::max();
static uint32 const INVALID_VERTEX_ID = INVALID_TRIP_ID;
// below numbers are memory bounded
static const uint32 MAX_STOPS = 8000;
static const uint32 MAX_TRIPS = 191939;

pid_t gettid()
{
	return syscall(__NR_gettid);
}

struct AssertionException
{};

void assert_(bool condition)
{
    if (not condition)
    {
        throw AssertionException();
    }
}

GtfsVertex::GtfsVertex(uint32 id, uint32 stopId, uint32 tripId, uint32 departureTime, uint32 arrivalTime) :
    id(id),
    stopId(stopId),
    tripId(tripId),
    departureTime(departureTime),
    arrivalTime(arrivalTime)
{
}

string GtfsVertex::toString() const
{
    stringstream out;
    out << "[id:" << this->id << ",stop:" << this->stopId << ",trip:" << this->tripId
        << ",dep:" << this->departureTime << ",arr:" << this->arrivalTime << "]";
    return out.str();
}

class StopVertex;

class GtfsVertexImpl
{
public:

    GtfsVertexImpl(uint32 id, StopVertex const * stop, uint32 tripId, uint32 departureTime, uint32 arrivalTime);

    vector<GtfsVertexImpl const *> getNeighbourVerticles(uint32 minChangeTime, uint32 maxChangeTime, uint32 serviceId) const;

    bool operator<(GtfsVertexImpl const & other) const;

    StopVertex const * stop;
    uint32 id;
    uint32 tripId;
    uint32 departureTime;
    uint32 arrivalTime;
};

struct StopVertex
{
    StopVertex()
    {
        assert_(false);
    }

    explicit StopVertex(uint32 id, double lat, double lon) :
        stopId(id),
        lat(lat),
        lon(lon)
    {
    }

    bool operator<(StopVertex const & other) const
    {
        return this->stopId < other.stopId;
    }

    uint32 stopId;
    double lat;
    double lon;
    std::vector<GtfsVertexImpl > edges;
};

struct GtfsRouteImpl
{
    GtfsRouteImpl(uint32 time, uint32 estimate);

    GtfsRoute toGtfsRoute() const;
    bool operator<(GtfsRouteImpl const & other) const;

    void compact()
    {
        if (this->verticles->size() == RVEC_MAX)
        {
            this->vectors.push_back(this->verticles);
            this->verticles = shared_ptr<vector<GtfsVertexImpl const *> >(new std::vector<GtfsVertexImpl const *>());
            this->verticles->reserve(RVEC_MAX);
        }
    }
    

    uint32 time;
    uint32 estimate;
    vector<shared_ptr<vector<GtfsVertexImpl const *> > > vectors;
    shared_ptr<vector<GtfsVertexImpl const *> > verticles;
};

struct GtfsVertexImplEquality
{
    bool operator()(GtfsVertexImpl const & a, GtfsVertexImpl const & b) const
    {
        return a.tripId == b.tripId && a.stop->stopId == b.stop->stopId;
    }

};


/******** ThreadPool **********/

class Thread : boost::noncopyable
{
public:
    explicit Thread(uint32 id);
    virtual ~Thread();

    void start();
    void join();

    virtual void run() = 0;

private:
    static void startRoutine(Thread &);

    uint32 const threadId;
    bool running;
    boost::mutex mutable mutex;
    boost::condition runCondition;
    boost::thread * threadImpl;
};

class ThreadPool : boost::noncopyable
{
public:
    typedef boost::function<void (void)> Task;

    explicit ThreadPool(uint32 numThreads);

    void executeTask(GtfsRouterImpl *);

    void stop(); // blocking

private:
    class WorkerThread;

    void threadEnded();

    uint32 numThreads;
    boost::ptr_list<WorkerThread> threads;
    boost::mutex mutable mainMutex;
    boost::condition taskCondition;
    std::deque<GtfsRouterImpl *> taskQueue;
    bool running;
};

void Thread::startRoutine(Thread & thread)
{
	thread.run();
}

Thread::Thread(uint32 id) :
	threadId(id),
	running(false),
	threadImpl(NULL)
{
}

Thread::~Thread()
{
	assert_(!this->threadImpl);
}

void Thread::start()
{
	assert_(!this->threadImpl);
	this->threadImpl = new boost::thread(boost::bind(&Thread::startRoutine, boost::ref(*this)));
}

void Thread::join()
{
	if (this->threadImpl)
	{
		
		this->threadImpl->join();
		delete this->threadImpl;
		this->threadImpl = NULL;
	}
}

/************************** Helpers *************************/

bool compareNodes(GtfsRouteImpl const * routeA, GtfsRouteImpl const * routeB)
{
    return routeA->time + routeA->estimate > routeB->time + routeB->estimate;
}

uint32 distance(double lat1, double lon1, double lat2, double lon2)
{
    Geodesic const & geod = Geodesic::WGS84;

    double s12;
    geod.Inverse(lat1, lon1, lat2, lon2, s12);
    return s12;
}

uint32 euclideanDistance(double lat1, double lon1, double lat2, double lon2)
{
    return sqrt(pow((lat1 - lat2) * 110992, 2) + pow((lon1 - lon2) * 88090, 2));
}

uint32 getWalkTimeFromTo(double lat1, double lon1, double lat2, double lon2)
{
    return distance(lat1, lon1, lat2, lon2) / (3000.0 / 3600);
}

inline double deg2rad(double deg)
{
    return deg * 0.017453292519943295;
}

uint32 distance2(double lat1, double lon1, double lat2, double lon2)
{
    return acos(sin(deg2rad(lat1))*sin(deg2rad(lat2))
        + cos(deg2rad(lat1))*cos(deg2rad(lat2))*cos(deg2rad(lon1)
        - deg2rad(lon2))) * EARTH_RADIUS_IN_M;
}

uint32 distance3(double lat1, double lon1, double lat2, double lon2)
{
    return sqrt(pow((lat1 - lat2) * 111, 2) + pow((lon1 - lon2) * 70, 2)) * 1000;
}

uint32 timeFromTo(double lat1, double lon1, double lat2, double lon2)
{
    return euclideanDistance(lat1, lon1, lat2, lon2) / 32;
}

/***************** GlobalData ***************/

typedef vector<StopVertex> StopsMap;
typedef vector<uint32> TripMap;

class GlobalData : boost::noncopyable
{
public:
    GlobalData(string const & dbname, string const & user, string const & host, string const & password);
    ~GlobalData();

    StopsMap allStops;
    TripMap allTrips;
    uint32 const numThreads;
    ThreadPool pool;
private:
    void uploadAllStop(pqxx::work &);
    void uploadAllTrips(pqxx::work &);
};

bool departuresEarlier(GtfsVertexImpl const & v1, GtfsVertexImpl const & v2)
{
    return v1.departureTime < v2.departureTime;
}

bool departuresEarlierThan(GtfsVertexImpl const & v, uint32 departureTime)
{
    return v.departureTime < departureTime;
}

GlobalData::GlobalData(string const & dbname, string const & user, string const & host, string const & password) :
    numThreads(boost::thread::hardware_concurrency()),
    pool(numThreads)
{
    pqxx::connection dbConnection("dbname= " + dbname + " user=" + user + " host=" + host + " password=" + password);

    pqxx::work connection(dbConnection);
    this->uploadAllTrips(connection);
    this->uploadAllStop(connection);

    pqxx::result result = connection.exec("SELECT id, stop_id, next_stop_id, trip_id, departure_timestamp,"\
                                            "arrival_timestamp FROM poc_edges2 ORDER BY id ASC");
    uint32 const numRows = result.size();

    for (uint32 i = 0; i < numRows; ++i)
    {
        uint32 const stopId = result[i][1].as<uint32>();
        uint32 const nextStopId = result[i][2].as<uint32>();

        this->allStops[stopId].edges.push_back(GtfsVertexImpl(
                        result[i][0].as<uint32>(),
                        &this->allStops[nextStopId],
                        result[i][3].as<uint32>(),
                        uint32(result[i][4].as<uint32>()),
                        uint32(result[i][5].as<uint32>())
                        )
        );
    }

    for (StopsMap::iterator stopIt = this->allStops.begin(); stopIt != this->allStops.end(); ++stopIt)
    {
        if (stopIt->stopId == 11)
            continue;
        sort(stopIt->edges.begin(), stopIt->edges.end(), departuresEarlier);
        vector<GtfsVertexImpl> tmp(stopIt->edges);
        tmp.swap(stopIt->edges);
    }

    dbConnection.disconnect();
}

void GlobalData::uploadAllTrips(pqxx::work & connection)
{
    pqxx::result result = connection.exec("SELECT trip_id, service_id FROM poc_trips");
    uint32 const numRows = result.size();
    this->allTrips.resize(MAX_TRIPS, INVALID_TRIP_ID);

    for (uint32 i = 0; i < numRows; ++i)
    {
        this->allTrips[result[i][0].as<uint32>() - FIRST_STOP_ID] = result[i][1].as<uint32>();
    }
}

void GlobalData::uploadAllStop(pqxx::work & connection)
{
    pqxx::result result = connection.exec("SELECT stop_id, stop_lat, stop_lon FROM poc_stops ORDER BY stop_id ASC");
    uint32 const numRows = result.size();
    this->allStops.resize(MAX_STOPS, StopVertex(INVALID_VERTEX_ID, 0.0, 0.0));

    for (uint32 i = 0; i < numRows; ++i)
    {
        uint32 const stopId = result[i][0].as<uint32>();
        this->allStops[stopId] = StopVertex(stopId, result[i][1].as<double>(), result[i][2].as<double>());
    }
}

GlobalData::~GlobalData()
{
    this->pool.stop();
}

boost::scoped_ptr<GlobalData> globalData(NULL);

void initialize(string const & dbname, string const & user, string const & host, string const & password)
{
    assert_(not globalData.get());
    globalData.reset(new GlobalData(dbname, user, host, password));
}

/**********************************************/

class RouteCallback : boost::noncopyable
{
public:
    RouteCallback(uint32 numCalls) :
        numCalls(numCalls)
    {
    }

    void operator()(boost::optional<GtfsRoute> const & nodes)
    {
        boost::mutex::scoped_lock lock(this->mutex);
        if (nodes)
        {
            this->results.insert(*nodes);
        }

        if (--this->numCalls == 0)
        {
            this->cond.notify_all();
        }
    }

    void wait()
    {
        boost::mutex::scoped_lock lock(this->mutex);
        while (0 != this->numCalls)
        {
            this->cond.wait(lock);
        }
    }

    multiset<GtfsRoute> results;

private:
    uint32 numCalls;
    boost::mutex mutex;
    boost::condition cond;
};

class GtfsRouterImpl : boost::noncopyable
{
public:

    GtfsRouterImpl(GtfsRouter & owner, RouteCallback & callback, RoutingAlgo algo,
                    Point const & startPoint, Point const & endPoint,
                    StopVertex const * startStop, StopVertex const * endStop,
                    uint32 tripStartTime, uint32 minChangeTime, uint32 maxChangeTime,
                    uint32 serviceId, uint32 maxLoops // FIXME: serviceId should be a collection (single value works for SF)
    ):
        owner(owner),
        callback(callback),
        algo(algo),
        startPoint(startPoint),
        endPoint(endPoint),
        startStop(startStop),
        endStop(endStop),
        tripStartTime(tripStartTime),
        minChangeTime(minChangeTime),
        maxChangeTime(maxChangeTime),
        serviceId(serviceId),
        maxLoops(maxLoops),
        routes(compareNodes)
    {
        this->open.resize(8000, 0);
        this->closed.resize(8000, 0);
    }

    void route()
    {
        this->callback(this->routeImpl());
        delete this;
    }

private:
    typedef bool (*F)(GtfsRouteImpl const *, GtfsRouteImpl const *);
    typedef std::priority_queue<GtfsRouteImpl *, vector<GtfsRouteImpl *>, F> RoutesQueue;
    typedef vector<uint32> StopsIdVector;

    boost::optional<GtfsRoute> routeImpl()
    {
        uint32 const walkTime = getWalkTimeFromTo(this->startPoint.first,
                                                    this->startPoint.second,
                                                    this->startStop->lat,
                                                    this->startStop->lon);

        GtfsVertexImpl const startVertex(FOOT_VERTEX_ID,
                                        this->startStop,
                                        FOOT_TRIP_ID,
                                        this->tripStartTime,
                                        this->tripStartTime + walkTime);

        GtfsRouteImpl * initialRoute = new GtfsRouteImpl(walkTime, 0);
        initialRoute->verticles->push_back(&startVertex);
        this->open[startVertex.stop->stopId] = walkTime;
        this->routes.push(initialRoute);

        for (uint32 i = 0; i < this->maxLoops; ++i)
        {
            if (this->routes.empty())
            {
                cout << "No Route, count " << i << endl;
                return boost::optional<GtfsRoute>();
            }

            GtfsRouteImpl & route = *this->routes.top();
            boost::scoped_ptr<GtfsRouteImpl> p(&route);

            if (route.time > this->owner.getMinTime())
            {
                return boost::optional<GtfsRoute>();
            }

            this->routes.pop();
            GtfsVertexImpl const * vertex = route.verticles->back();

            if (this->open[vertex->stop->stopId] < route.time)
            {
                continue;
            }
            assert_(this->open[vertex->stop->stopId] == route.time);

            this->closed[vertex->stop->stopId] = route.time;
            this->open[vertex->stop->stopId] = 0;

            if (vertex->stop->stopId == this->endStop->stopId)
            {
                cout << " Success in " << i << " counts, TIME: " << route.time
                    << ", verticles: " << route.verticles->size() << endl;

                uint32 const walkTime = getWalkTimeFromTo(this->endStop->lat,
                                                            this->endStop->lon,
                                                            this->endPoint.first,
                                                            this->endPoint.second);
                route.time += walkTime;
                GtfsRoute result = route.toGtfsRoute();
                result.verticles.push_back(GtfsVertex(FOOT_VERTEX_ID, 0,
                                                    FOOT_TRIP_ID,
                                                    vertex->arrivalTime,
                                                    vertex->arrivalTime + walkTime));

                this->owner.trySetMinTime(route.time);

                return result;
            }

            vector<GtfsVertexImpl const *> const & neighbours = vertex->getNeighbourVerticles(
                                                                                this->minChangeTime,
                                                                                this->maxChangeTime,
                                                                                this->serviceId);
            route.compact();

            for (vector<GtfsVertexImpl const *>::const_iterator next = neighbours.begin(); next != neighbours.end(); ++next)
            {
                this->addToQueue(vertex, *next, route);
            }
        }
        cout << "No Route: count out" << endl;
        return boost::optional<GtfsRoute>();
    }

    void addToQueue(GtfsVertexImpl const * vertex, GtfsVertexImpl const * nextVertex, GtfsRouteImpl const & route)
    {
        uint32 const time = nextVertex->arrivalTime - this->tripStartTime;

        if (this->closed[nextVertex->stop->stopId] and time >= this->closed[nextVertex->stop->stopId])
        {
            return;
        }
        if (this->open[nextVertex->stop->stopId] and time >= this->open[nextVertex->stop->stopId])
        {
            return;
        }

        uint32 estimate = 0;
        if (this->algo == ASTAR)
        {
            estimate = timeFromTo(this->endStop->lat, this->endStop->lon, nextVertex->stop->lat, nextVertex->stop->lon);
        }

        GtfsRouteImpl * newRoute = new GtfsRouteImpl(time, estimate);
        *(newRoute->verticles) = *route.verticles; // TODO: optimize this copy
        newRoute->vectors = route.vectors;
        newRoute->verticles->push_back(nextVertex);

        this->open[nextVertex->stop->stopId] = newRoute->time;

        this->routes.push(newRoute);
    }

    GtfsRouter & owner;
    RouteCallback & callback;
    RoutingAlgo const algo;
    Point const startPoint;
    Point const endPoint;
    StopVertex const * startStop;
    StopVertex const * endStop;
    uint32 tripStartTime;
    uint32 const minChangeTime;
    uint32 const maxChangeTime;
    uint32 const serviceId;
    uint32 const maxLoops;

    RoutesQueue routes;
    StopsIdVector closed;
    StopsIdVector open;
};

GtfsVertexImpl::GtfsVertexImpl(uint32 id, StopVertex const * stop, uint32 tripId, uint32 departureTime, uint32 arrivalTime) :
    stop(stop),
    id(id),
    tripId(tripId),
    departureTime(departureTime),
    arrivalTime(arrivalTime)
{
}

vector<GtfsVertexImpl const *> GtfsVertexImpl::getNeighbourVerticles(uint32 minChangeTime,
                                                                    uint32 maxChangeTime,
                                                                    uint32 serviceId) const
{
    std::vector<GtfsVertexImpl const *> edges;
    edges.reserve(128);
    
    vector<GtfsVertexImpl>::const_iterator it = 
                lower_bound(this->stop->edges.begin(), this->stop->edges.end(), this->arrivalTime, departuresEarlierThan);

    for (; it != this->stop->edges.end(); ++it)
    {
        if (globalData->allTrips[it->tripId - FIRST_STOP_ID] != serviceId)
            continue;
        if (it->tripId != this->tripId and it->departureTime < this->arrivalTime + minChangeTime)
            continue;
        if (it->departureTime > this->arrivalTime + maxChangeTime)
            break;
        edges.push_back(&(*it));
    }
    return edges;
}

bool GtfsVertexImpl::operator<(GtfsVertexImpl const & other) const
{
    return this->id < other.id;
}


GtfsRouteImpl::GtfsRouteImpl(uint32 time, uint32 estimate) :
    time(time),
    estimate(estimate),
    verticles(new vector<GtfsVertexImpl const *>())
{
    this->verticles->reserve(RVEC_MAX);
}

GtfsRoute GtfsRouteImpl::toGtfsRoute() const 
{
    GtfsRoute result(this->time);

    BOOST_FOREACH(shared_ptr<vector<GtfsVertexImpl const *> > const sp, this->vectors)
    {
        {
            BOOST_FOREACH(GtfsVertexImpl const * v, *sp)
            {
                result.verticles.push_back(GtfsVertex(v->id, v->stop->stopId, v->tripId, v->departureTime, v->arrivalTime));
            }
        }
    }

    BOOST_FOREACH(GtfsVertexImpl const * v, *this->verticles)
    {
        result.verticles.push_back(GtfsVertex(v->id, v->stop->stopId, v->tripId, v->departureTime, v->arrivalTime));
    }
    return result;
}

bool GtfsRouteImpl::operator<(GtfsRouteImpl const & other) const
{
    if (this->time != other.time)
        return this->time < other.time;
    else
        return this->verticles->size() < other.verticles->size();
}

void getStopsDistancesFrom(
    Point const & startPoint, std::multimap<uint32, StopVertex const *> & startDistances,
    Point const & endPoint, std::multimap<uint32, StopVertex const *> & endDistances)
{
    for (StopsMap::const_iterator stopIt = globalData->allStops.begin(); stopIt != globalData->allStops.end(); ++stopIt)
    {
        if (stopIt->stopId == 11)
            continue;
        StopVertex const * stop = &(*stopIt);
        startDistances.insert(make_pair(distance(startPoint.first, startPoint.second, stop->lat, stop->lon), stop));
        endDistances.insert(make_pair(distance(endPoint.first, endPoint.second, stop->lat, stop->lon), stop));
    }
}

/************* GtfsRouter ***************/

GtfsRouter::GtfsRouter(RoutingAlgo algo, Point startPoint, Point endPoint,
                        int32 tripStartTime, int32 numRoutesToReturn,
                        int32 numStartStops, int32 numEndStops,
                        int32 minChangeTime, int32 maxChangeTime,
                        int32 serviceId
) :
    minTime(std::numeric_limits<uint32>::max()),
    algo(algo),
    startPoint(startPoint),
    endPoint(endPoint),
    tripStartTime(tripStartTime),
    numRoutesToReturn(numRoutesToReturn),
    numStartStops(numStartStops),
    numEndStops(numEndStops),
    minChangeTime(minChangeTime),
    maxChangeTime(maxChangeTime),
    serviceId(serviceId),
    maxLoops(1500)
{
}

void GtfsRouter::trySetMinTime(uint32 time)
{
    boost::mutex::scoped_lock lock(*this->minTimeMtx);
    if (time < this->minTime)
    {
        this->minTime = time;
    }
}

uint32 GtfsRouter::getMinTime() const
{
    boost::mutex::scoped_lock lock(*this->minTimeMtx);
    return this->minTime;
}

RoutesList GtfsRouter::route()
{
    this->minTimeMtx = new boost::mutex;
    boost::scoped_ptr<boost::mutex> tmp(this->minTimeMtx);
    typedef multimap<uint32, StopVertex const *>::const_iterator DistanceMapIter;

    multimap<uint32, StopVertex const *> startDistances;
    multimap<uint32, StopVertex const *> stopDistances;


    getStopsDistancesFrom(this->startPoint, startDistances, this->endPoint, stopDistances);

    uint32 const numLoops = this->numStartStops * this->numEndStops ;  // TODO min of stops and user request
    RouteCallback cb(numLoops);

    uint32 numStarts = this->numStartStops;
    for (DistanceMapIter startStopIt = startDistances.begin(); startStopIt != startDistances.end() && numStarts; ++startStopIt, --numStarts)
    {
        uint32 numEnds = this->numEndStops;
        for (DistanceMapIter endStopIt = stopDistances.begin(); endStopIt != stopDistances.end() && numEnds; ++endStopIt, --numEnds)
        {
            StopVertex const * startStop = startStopIt->second;
            StopVertex const * endStop = endStopIt->second;

            GtfsRouterImpl * router = new GtfsRouterImpl(*this, cb, this->algo, this->startPoint, this->endPoint,
                                                        startStop, endStop, this->tripStartTime, this->minChangeTime,
                                                        this->maxChangeTime, this->serviceId, this->maxLoops);
            globalData->pool.executeTask(router);
        }
    }

    cb.wait();

    RoutesList ret;
    for (multiset<GtfsRoute>::const_iterator it = cb.results.begin(); it != cb.results.end(); ++it)
    {
        if (ret.size() == this->numRoutesToReturn)
            break;
        ret.push_back(*it);
    }

    return ret;
}

BOOST_PYTHON_MODULE(gtfsrouting)
{
    using namespace py;
    
    enum_<RoutingAlgo>("RoutingAlgo")
        .value("DIJKSTRA", DIJKSTRA)
        .value("ASTAR", ASTAR)
    ;

    class_<std::pair<double, double> >("Point", init<double, double>())
        .def_readwrite("lat", &std::pair<double, double>::first)
        .def_readwrite("lon", &std::pair<double, double>::second);

    class_<GtfsRouter>("GtfsRouter", init<RoutingAlgo, Point, Point, int32, int32, int32, int32, int32, int32, int32>())
        .def("route", &GtfsRouter::route)
    ;

    class_<GtfsVertex>("GtfsVertex", init<int32, int32, int32, int32, int32>())
        .def_readonly("id", &GtfsVertex::id)
        .def_readonly("stopId", &GtfsVertex::stopId)
        .def_readonly("tripId", &GtfsVertex::tripId)
        .def_readonly("departureTime", &GtfsVertex::departureTime)
        .def_readonly("arrivalTime", &GtfsVertex::arrivalTime)
        .def("__str__", &GtfsVertex::toString)
        .def("__unicode__", &GtfsVertex::toString)
    ;

    class_<GtfsRoute>("GtfsRoute", init<int32>())
        .def_readonly("time", &GtfsRoute::time)
        .def_readonly("verticles", &GtfsRoute::verticles)
    ;

    class_<RoutesList>("RoutesList")
        .def(vector_indexing_suite<RoutesList>())
    ;

    class_<VerticlesList>("VerticlesList")
        .def(vector_indexing_suite<VerticlesList>())
    ;

    boost::python::scope().attr("FOOT_TRIP_ID") = FOOT_TRIP_ID;
    boost::python::scope().attr("FOOT_VERTEX_ID") = FOOT_VERTEX_ID;

    def("initialize", initialize);
}

class ThreadPool::WorkerThread : public Thread
{
public:
	WorkerThread(ThreadPool & pool, uint32 threadId) :
		Thread(threadId),
		pool(pool)
	{
	}

	void run()
	{
		while (true)
		{
			GtfsRouterImpl * router = this->getTask(); // blocking
			if (!router)
			{
				this->pool.threadEnded();
				return;
			}
			router->route(); // do the job
		}
	}

private:
	GtfsRouterImpl * getTask()
	{
        boost::mutex::scoped_lock lock(this->pool.mainMutex);

        while (this->pool.running)
        {
            if (this->pool.taskQueue.empty())
            {
                this->pool.taskCondition.wait(lock);
            }
            else
			{
                GtfsRouterImpl * task = this->pool.taskQueue.front();
                this->pool.taskQueue.pop_front();
                return task;
            }
        }
        return NULL;
    }

    ThreadPool & pool;
};

ThreadPool::ThreadPool(uint32 numThreads) :
    numThreads(numThreads),
    running(true)
{
    for (uint32 i = 0; i < this->numThreads; ++i)
    {
        WorkerThread * const thread = new WorkerThread(*this, i); 
        this->threads.push_back(thread);
        thread->start();
    }
}

void ThreadPool::executeTask(GtfsRouterImpl * router)
{
    boost::mutex::scoped_lock lock(this->mainMutex);

    bool const wasEmpty = this->taskQueue.empty();
    this->taskQueue.push_back(router);
    if (wasEmpty)
	{
        this->taskCondition.notify_one();
    }
}

void ThreadPool::threadEnded()
{
    boost::mutex::scoped_lock lock(this->mainMutex);	

    --this->numThreads;
    if (!this->numThreads)
    {
        this->taskCondition.notify_one();
    }
}

void ThreadPool::stop()
{
    boost::mutex::scoped_lock lock(this->mainMutex);
    this->running = false;
    this->taskCondition.notify_all();

    while (this->numThreads)
    {
        this->taskCondition.wait(lock);
    }

    boost::ptr_list<WorkerThread>::iterator endIt = this->threads.end();
    for (boost::ptr_list<WorkerThread>::iterator it = this->threads.begin(); it != endIt; ++it)
    {
	    it->join();
    }

    this->threads.clear(); // delete the threads
}
                                                                                                                       
