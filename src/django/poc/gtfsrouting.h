#ifndef __GTFSROUTING_H
#define __GTFSROUTING_H

#include <list>
#include <queue>
#include <stdint.h>
#include <set>
#include <deque>
#include <vector>
#include <map>
#include <memory>
#include <pqxx/pqxx>
#include <boost/bind.hpp>
#include <boost/python.hpp>
#include <boost/python/list.hpp>
#include <boost/thread.hpp>

typedef int32_t int32;
typedef uint32_t uint32;
typedef std::pair<double, double>  Point;

uint32 const FOOT_VERTEX_ID = 1073741824;
uint32 const FOOT_TRIP_ID = FOOT_VERTEX_ID;

void initialize(std::string const & dbname, std::string const & user, std::string const & host, std::string const & password);

struct GtfsVertex
{
    GtfsVertex(uint32 id, uint32 stopId, uint32 tripId, uint32 departureTime, uint32 arrivalTime);

    std::string toString() const;
    bool operator==(GtfsVertex const & other) const
    {
        return this->id == other.id;
    }
    
    uint32 id;
    uint32 stopId;
    uint32 tripId;
    uint32 departureTime;
    uint32 arrivalTime;
};

typedef std::vector<GtfsVertex> VerticlesList;
struct GtfsRoute
{
    GtfsRoute(uint32 time) :
        time(time)
    {}

    bool operator<(GtfsRoute const & other) const
    {
        return this->time < other.time;
    }

    bool operator==(GtfsRoute const & other) const
    {
        return this->time == other.time and this->verticles == other.verticles;
    }

    uint32 time;
    VerticlesList verticles;
};

typedef enum
{
    ASTAR = 65,
    DIJKSTRA = 68,
} RoutingAlgo;

class StopVertex;
typedef std::vector<GtfsRoute> RoutesList;
class GtfsRouterImpl;

class GtfsRouter
{
public:

    GtfsRouter(RoutingAlgo algo, Point startPoint, Point endPoint,
                int32 tripStartTime, int32 numRoutesToReturn,
                int32 numStartStops, int32 numEndStops,
                int32 minChangeTime, int32 maxChangeTime,
                int32 serviceId
    );

    RoutesList route();

private:
    friend class GtfsRouterImpl;

    uint32 getMinTime() const;
    void trySetMinTime(uint32 time);

    boost::mutex * minTimeMtx;
    uint32 minTime;
    RoutingAlgo const algo;
    Point const startPoint;
    Point const endPoint;
    uint32 const tripStartTime;
    uint32 const numRoutesToReturn;
    uint32 const numStartStops;
    uint32 const numEndStops;
    uint32 const minChangeTime;
    uint32 const maxChangeTime;
    uint32 const serviceId;
    uint32 const maxLoops;
};

#endif //__GTFSROUTING_H
