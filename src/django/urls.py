from django.conf.urls.defaults import patterns, include, url
from geodjango.poc import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'geodjango.views.home', name='home'),
    # url(r'^geodjango/', include('geodjango.foo.urls')),

    url(r'^findroute/', views.findroute),
    url(r'^reportpos/', views.reportpos),
    url(r'^nextbusreport/', views.nextbusreport),
)
